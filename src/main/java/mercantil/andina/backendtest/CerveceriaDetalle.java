package mercantil.andina.backendtest;

import java.util.Date;

public class CerveceriaDetalle {
	
	public int id;
    public String obdb_id;
    public String name;
    public String brewery_type;
    public String street;
    public Object address_2;
    public Object address_3;
    public String city;
    public String state;
    public Object county_province;
    public String postal_code;
    public String country;
    public String longitude;
    public String latitude;
    public String phone;
    public String website_url;
    public Date updated_at;
    public Date created_at;
    
    
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getObdb_id() {
		return obdb_id;
	}
	public void setObdb_id(String obdb_id) {
		this.obdb_id = obdb_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBrewery_type() {
		return brewery_type;
	}
	public void setBrewery_type(String brewery_type) {
		this.brewery_type = brewery_type;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public Object getAddress_2() {
		return address_2;
	}
	public void setAddress_2(Object address_2) {
		this.address_2 = address_2;
	}
	public Object getAddress_3() {
		return address_3;
	}
	public void setAddress_3(Object address_3) {
		this.address_3 = address_3;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Object getCounty_province() {
		return county_province;
	}
	public void setCounty_province(Object county_province) {
		this.county_province = county_province;
	}
	public String getPostal_code() {
		return postal_code;
	}
	public void setPostal_code(String postal_code) {
		this.postal_code = postal_code;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getWebsite_url() {
		return website_url;
	}
	public void setWebsite_url(String website_url) {
		this.website_url = website_url;
	}
	public Date getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
	@Override
	public String toString() {
		return "CerveceriaDetalle [id=" + id + ", obdb_id=" + obdb_id + ", name=" + name + ", brewery_type="
				+ brewery_type + ", street=" + street + ", address_2=" + address_2 + ", address_3=" + address_3
				+ ", city=" + city + ", state=" + state + ", county_province=" + county_province + ", postal_code="
				+ postal_code + ", country=" + country + ", longitude=" + longitude + ", latitude=" + latitude
				+ ", phone=" + phone + ", website_url=" + website_url + ", updated_at=" + updated_at + ", created_at="
				+ created_at + "]";
	}    
    

}
