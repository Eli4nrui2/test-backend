package mercantil.andina.backendtest;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;



public class EjercicioTest {
	Cerveceria cerveceriaCalifornia;
	
	@Test
	public void backendEjercicio1y2Test() throws Exception {
		String url = "https://api.openbrewerydb.org/breweries/autocomplete?query=lagunitas";
		String cerveceriasResult = "";
		try {
			// llamado a la api
			cerveceriasResult = peticionHttpGet(url);
			// respuesta de api convertida a jsonList
			JSONArray jsonList = new JSONArray(cerveceriasResult);
			// creo arreglo de cervecerias donde voy a meter las que cumplen condicion
			List<Cerveceria> cervecerias = new ArrayList<Cerveceria>();
			// recorro la respuesta del servicio
			for (int i = 0 ; i<jsonList.length(); i++) {
				Cerveceria cerveceria = null;
				// seteo los valores de cerveceria del json en el objeto
				cerveceria = new Gson().fromJson(jsonList.get(i).toString(), Cerveceria.class);
				// evaluo si la cerveceria cumple con la condicion de nombre del enunciado
				if (cerveceria.getName().equals("Lagunitas Brewing Co")) {
					// si cumple la agrego al listado de cervecerias
					cervecerias.add(cerveceria);
				}			
			}		
			// de antemano se que 2 cervecerias cumplen la condicion del enunciado, entonces
			// evaluo las cervecerias seleccionadas con el valos que espero
			Assert.assertTrue("Las cervecerias que coinciden con el nombre son 2", cervecerias.size() == 2);
			
		} catch (Exception e) {
			// Manejar excepci�n
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void backendEjercicio3Test() {
		String url = "https://api.openbrewerydb.org/breweries/autocomplete?query=lagunitas";
		String cerveceriasResult = "";
		String url2 = " https://api.openbrewerydb.org/breweries/";
		String cerveceriasDetalleResult = "";
		try {
			// llamado a la api
			cerveceriasResult = peticionHttpGet(url);
			// respuesta de api convertida a jsonList
			JSONArray jsonList = new JSONArray(cerveceriasResult);
			// creo arreglo de cervecerias donde voy a meter las que cumplen condicion
			List<Cerveceria> cervecerias = new ArrayList<Cerveceria>();
			// recorro la respuesta del servicio
			for (int i = 0; i < jsonList.length(); i++) {
				Cerveceria cerveceria = null;
				// seteo los valores de cerveceria del json en el objeto
				cerveceria = new Gson().fromJson(jsonList.get(i).toString(), Cerveceria.class);
				// evaluo si la cerveceria cumple con la condicion de nombre del enunciado
				if (cerveceria.getName().equals("Lagunitas Brewing Co")) {
					// llamado a la api de detalle
					cerveceriasResult = peticionHttpGet(url2 + cerveceria.getId());
					// respuesta de api de detalle convertida a jsonList
					JSONObject jsonObject = new JSONObject(cerveceriasResult);
					Cerveceria cerveceriaDetalle = new Gson().fromJson(jsonObject.toString(), Cerveceria.class);
					// si cumple la agrego al listado de cervecerias
					if (cerveceriaDetalle.getState().contains("California") == true) {
						// si cumple la agrego al listado de cervecerias
						cervecerias.add(cerveceria);	
					}									
				}			
			}
			
			// de antemano se que 2 cervecerias cumplen la condicion del enunciado, entonces
			// evaluo las cervecerias seleccionadas con el valos que espero
			Assert.assertTrue("Las cervecerias que coinciden con el state son 1", cervecerias.size() == 1);
			
		} catch (Exception e) {
			// Manejar excepci�n
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void backendEjercicio4Test() {
		String url = "https://api.openbrewerydb.org/breweries/autocomplete?query=lagunitas";
		String cerveceriasResult = "";
		String url2 = " https://api.openbrewerydb.org/breweries/";
		String cerveceriasDetalleResult = "";
		try {
			// llamado a la api
			cerveceriasResult = peticionHttpGet(url);
			// respuesta de api convertida a jsonList
			JSONArray jsonList = new JSONArray(cerveceriasResult);
			// creo arreglo de cervecerias donde voy a meter las que cumplen condicion
			List<Cerveceria> cervecerias = new ArrayList<Cerveceria>();
			// recorro la respuesta del servicio
			for (int i = 0; i < jsonList.length(); i++) {
				Cerveceria cerveceria = null;
				// seteo los valores de cerveceria del json en el objeto
				cerveceria = new Gson().fromJson(jsonList.get(i).toString(), Cerveceria.class);
				// evaluo si la cerveceria cumple con la condicion de nombre del enunciado
				if (cerveceria.getName().equals("Lagunitas Brewing Co")) {
					// llamado a la api de detalle
					cerveceriasResult = peticionHttpGet(url2 + cerveceria.getId());
					// respuesta de api de detalle convertida a jsonList
					JSONObject jsonObject = new JSONObject(cerveceriasResult);
					Cerveceria cerveceriaDetalle = new Gson().fromJson(jsonObject.toString(), Cerveceria.class);
					// si cumple la agrego al listado de cervecerias
					if (cerveceriaDetalle.getState().contains("California") == true) {
						// si cumple la agrego al listado de cervecerias
						cervecerias.add(cerveceriaDetalle);	
					}									
				}			
			}
			
			// de antemano se que 2 cervecerias cumplen la condicion del enunciado, entonces
			// evaluo las cervecerias seleccionadas con el valos que espero
			Assert.assertTrue("Las cervecerias que coinciden con el state son 1", cervecerias.size() == 1);
			// como se de antemano que solo tengo que comparar el valor de 1 solo registro, que es el que cumple la condicion
			// del punto 3, utilizo el arreglo en posicion cero 0
			Assert.assertEquals(761, cervecerias.get(0).getId());
			Assert.assertEquals("Lagunitas Brewing Co", cervecerias.get(0).getName());
			Assert.assertEquals("1280 N McDowell Blvd", cervecerias.get(0).getStreet());
			Assert.assertEquals("7077694495", cervecerias.get(0).getPhone());
		} catch (Exception e) {
			// Manejar excepci�n
			e.printStackTrace();
		}
	}
	


	public static String peticionHttpGet(String urlParaVisitar) throws Exception {
		
		final ObjectMapper objectMapper = new ObjectMapper();
		// Esto es lo que vamos a devolver
		StringBuilder resultado = new StringBuilder();
		// Crear un objeto de tipo URL
		URL url = new URL(urlParaVisitar);
		// Abrir la conexi�n e indicar que ser� de tipo GET
		HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
		conexion.setRequestMethod("GET");
		// B�feres para leer
		BufferedReader rd = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
		String linea;
		// Mientras el BufferedReader se pueda leer, agregar contenido a resultado
		while ((linea = rd.readLine()) != null) {
			resultado.append(linea);
		}
		// Cerrar el BufferedReader
		rd.close();
		// Regresar resultado, pero como cadena, no como StringBuilder
		return resultado.toString();
	}
	
}
